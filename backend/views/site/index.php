<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Beranda</h1>
        <p class="lead">POST Anda</p>
    </div>

    <div class="body-content">

        <div class="row">
           <?php foreach ($post as $ps) { ?>
            <div class="col-lg-4">
                <h2><?= $ps->title ?></h2>
                <p>
                    <?= $ps->content ?>
                </p>
                <p><i class="btn btn-outline-secondary"><?= $ps->username ?></i></p>
            </div>
        <?php } ?>
    </div>

</div>
</div>
